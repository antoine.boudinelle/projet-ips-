
all: main

main: source/main.cpp $(OBJ)
	cd source && $(MAKE)

doc:
	cd source && $(MAKE) doc

clean:
	cd source && $(MAKE) clean

re: clean all 
