# Projet d'IPS - ENSIIE - 2019
*Charles Anteunis - Antoine Boudinelle*

## Objectif

Le but de ce projet est de calculer les solutions analytiques de l'oscillateur harmonique quantique à 1 Dimension

## Documentation

La documentation du projet est disponible au format html en accédant depuis un naviguateur au fichier `docs/html/index.html`. Elle peut être regénérée via :
```sh
make doc
```

## Prérequis

La compilation et l'exécution nécessite armadillo, matplotlib, numpy et python3.

## Exécutables

Les tests peuvent être compilés, puis lancés, depuis le dossier source :
```sh
make tests
./source/testSolution
```

L'exécutable principapl du calcul de la solution se compile et se lance de la même manière :
```sh
make
./source/oscillateur
```
Cela va remplir un fichier `values_phi.txt` qui contient les valeurs de la solutions selon les paramètres renseignés dans le `main.cpp`. Les différents graphiques des solutions peuvent être affichés via le fichier `affichage.py` et en lançant les commandes :
```sh
make
./source/oscillateur
python3 source/affichage.py
```

Pour repartir avec un dossier projet propre, vous pouvez exécuter `make clean`.
