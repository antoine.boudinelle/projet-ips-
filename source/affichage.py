import matplotlib.pyplot as plt
import numpy as np

# on charge les valeurs calculées de la solution
f = open('values_phi.txt')

y = []
# on ajoute les valeurs dans la liste y
for line in f.readlines():
    y.append([float(n) for n in line.split(';')[:-1]])

x = [i for i in np.linspace(-10,10,1000)]

# plot le y_n souhaité
plt.plot(x,y[2])
plt.show()
