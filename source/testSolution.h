#ifndef TESTSOLUTION_H
#define TESTSOLUTION_H

#include "orthonormalite.h"
#include "solution.h"
#include <testSolution.h>

/**
 * \file testSolution.h
 * \brief tests unitaires des différentes fonctions de calcul de la solution
 * \author Charles Anteunis
 */

class TestSolution : public CxxTest::TestSuite
{
public:
    /**
     * \brief test si $H_n(0) = 0$
     */
    void testHermite0_2(void)
    {
        arma::Row<float> C = {2, 3};
	arma::Mat<float> tmp = hermite(5,C);
        TS_ASSERT_EQUALS(tmp.at(0,0), 1);
    }

    /**
     * \brief test si $H_4(3) = la bonne valeur$
     */
    void testHermite1_3(void)
    {
        arma::Row<float> C = {3};
        TS_ASSERT_EQUALS(hermite(5,C).at(1,0), 6);
    }

    /**
     * \brief test la valeur de $\psi_1(3)$
     */
    void testSolution(void)
    {
	arma::Row<float> vect_z = {3,2};
	arma::Mat<float> hn = hermite(3, vect_z);
        float m = 5*pow(10,-43);
        float omega = 1*pow(10,7);
        float alpha = m*omega/arma::datum::h_bar;
        float y = sqrt(alpha)*3;
        float psi_1_3 = pow(alpha/arma::datum::pi, 1/4)*sqrt(2)*y*exp(-(y*y)/2);
        arma::Mat<float> C = phi(3, vect_z, m, omega, sqrt(m*omega/arma::datum::h_bar)*hn);
        C.print();
	printf("real: %f, calculated: %f\n", psi_1_3, C(0,1));
	TS_ASSERT_EQUALS(C.at(1,0), psi_1_3);
    }

       void test_n_m(void)
    {
        int n = 5;
        arma::Row<float> z = {-1,0,1,2,3,4};
        float m = pow(10,-15);
        float w = pow(10,4);
       TS_ASSERT_EQUALS(integrale(n, z, phi(n, z, m, w, hermite(n, z)), m, w), 1);
    }

};

#endif
