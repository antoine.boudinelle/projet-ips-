#ifndef ENERGIE_H
#define ENERGIE_H

#include <armadillo>

/**
 * \file energie.h
 * \author Charles Anteunis
 */

float energie(arma::Row<float> z);

#endif
