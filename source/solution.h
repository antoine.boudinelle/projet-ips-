#ifndef SOLUTION_H
#define SOLUTION_H
#include <armadillo>

/**
 * \file solution.h
 * \author Charles Anteunis
 */

arma::Mat<float> hermite(int n, arma::Row<float> z);

arma::Mat<float> phi(int n,arma::Row<float> z, float m, float w, arma::Mat<float> hn);

#endif
