#include <stdio.h>
#include <iostream>
#include <fstream>

#include "solution.h"

using namespace std;

/**
 * \file main.cpp
 * \brief programme principal
 * \author Charles Anteunis
 * \author Antoine Boudinelle
 */

int main() {
	arma::Mat<float> hermite_tab;
	int i = 0, j = 0;
	int n = 20;
	int nb_z;
	ofstream f;

	f.open ("values_phi.txt");

	arma::Mat<float> res;
	arma::Row<float> z = arma::Row<float>(arma::conv_to<arma::Row<float>>::from(arma::linspace(-10,10,1000)));
	nb_z = z.size();
	float m = pow(10,-32), w = pow(10,-2);
	
	hermite_tab = hermite(n, z);
	res = phi(n, z, m, w, sqrt(m*w/arma::datum::h_bar)*hermite_tab); 
	for (i=0 ; i<n ; i++) {
		for (j=0 ; j<nb_z ; j++) {
			f << res(i,j) << ";";
		}
		f << "\n";
	}
	f.close();
	return 0;
}
