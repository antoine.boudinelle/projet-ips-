#ifndef ORTHO_H
#define ORTHO_H

#include "solution.h"
#include <math.h>

 /**
 * \brief calcule toutes les intégrale phi_m * phi_p ( avec m et p inférieur à n) pour vérifier l'orthonormalité
 * \param n indice de la dernière solution (phi_n)
 * \param z les points en lequel sont évaluées les solutions
 * \param phi contient les valeurs de phi_i (pour tout i allant de 0 à n) évalué en z 
 * \param m correspond à la valeur arbitraire de m
 * \param w correspond à la valeur arbitraire de w
 * \return 1 si p = n(orthonormales), 0 sinon
 */
float integrale(int n, arma::Row<float> z, arma::Mat<float> phi, float m, float w);


#endif
