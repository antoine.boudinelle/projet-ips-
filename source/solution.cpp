#include "solution.h"
#include<stdio.h>

/**
 * \file solution.cpp
 * \brief fonctions de calcul des solutions de l'oscillateur 1D
 * \author Antoine Boudinelle
 * \author Charles Anteunis
 */

/**
 * \brief Calcul par récurrence les polynomes de Hermite jusqu'a n
 * \param n : le degré maximal des polynomes, n doit être supérieur à 2
 * \param z : la position dans l'espace 1D
 * \return Retourne une matrice de taille n*m (avec m = |z|), chaque élément correspondant à la valeur de Hi(k) (i allant de 0 à n et \f$k \in z\f$)
 * \attention n doit être supérieur à 2
 */
arma::Mat<float> hermite(int n, arma::Row<float> z){
  int nb_z = z.size();
  arma::Mat<float> res = arma::Mat<float>(n,nb_z,arma::fill::zeros);
  arma::Col<float> res_aux = arma::Col<float>(n);
  int i , j ;
  for(j = 0;j < nb_z;j++){
    res_aux[0] = 1;
    res_aux[1] = 2 * z[j];
    for(i = 2;i < n;i++){
      res_aux[i] = ((2 * z[j] * res_aux[i-1]) - (2 * (n-1) * res_aux[i-2]));
    }
    res.col(j) = res_aux;
  }
  return res;
}

/**
 * \brief Calcul la valeur \f$\frac{1}{\sqrt{2^n*n!}}\f$
 * \param n la valeur j'usqu'où on souhaite calculer
 * \return Retourne un tableau de toutes les valeurs \f$\frac{1}{\sqrt{2^i*i!}}\f$ pour i allant de 0 à n
 */
arma::Row<float> constante(int n){
  arma::Row<float> res = arma::Row<float>(n+1);;
  int i;
  res(0) = 1;
  for (i = 1;i < n+1;i++){
    res(i) = res(i-1)*(1/(sqrt(2*i)));
  }
  return res;
}

/**
 * \brief Calcul d'une solution a l'équation de Schrödinger 
 * \param n : le degré maximal du polynome
 * \param z : les positions dans l'espace 1D
 * \param m : la masse de l'objet
 * \param w : la période d'oscillation 
 * \param hn : le tableaux des polynomes d'Hermite évalués en \f$\sqrt{m\frac{w}{h}}\times z\f$
 * \return Retourne les valeurs de phi évalué aux valeurs de z pout tous les indices de 0 à n
 */
arma::Mat<float> phi(int n,arma::Row<float> z, float m, float w, arma::Mat<float> hn){
  arma::Row<float> cst1 = constante(n);
  float cst2 = pow( (m * w)/(arma::datum::pi * arma::datum::h_bar), 1/4);
  float cst3 = m * w / (2 * arma::datum::h_bar);
  arma::Mat<float> res;
  arma::Row<float> res_aux;
  int i = 0;
  for (i = 0; i < n; i++){
    res_aux = arma::pow(z, 2);
    res_aux = arma::exp(res_aux * (-1) * cst3) * cst2 * cst1(i);
    res_aux = res_aux % hn.row(i);
    res.insert_rows(i, res_aux);
  }
  return res;
}
