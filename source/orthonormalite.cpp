#include "orthonormalite.h"

/**
 * \brief calcule la fonction de poids pour un polynome d'Hermite
 * \param x le point en lequel la fonction est évaluée
 * \return la valeur de \f$\omega(x)\f$
 */
float weight(float x){
	return exp(-x*x);
}

/**
 * \brief calcule toutes les intégrales \f$\int{\psi_m(z)*\psi_p(z)}dz\f$ ( avec m et p inférieur à n) pour vérifier l'orthonormalité
 * \param n indice de la dernière solution (phi_n)
 * \param z les points en lequel sont évaluées les solutions
 * \param phi contient les valeurs de phi_i (pour tout i allant de 0 à n) évalué en z 
 * \param m correspond à la valeur arbitraire de m
 * \param w correspond à la valeur arbitraire de w
 * \return 1 si les solutions sont orthonormales, 0 sinon
 */
float integrale(int n, arma::Row<float> z, arma::Mat<float> phi, float m, float w) {
	/*arma::Mat<float> hp = hermite(p,zi);
	arma::Mat<float> hn = hermite(n,zi); */
	int i,j,k;
	float sum;
	float epsilon = pow(10,-7);

	/*Décalage dû au changement de variable */
	float c = sqrt(m * w / arma::datum::h_bar);

	for (i = 0;i < n; i++){
		for (j = 0; j<n;j++){ /* i et j sont les indices de phi */
		sum = 0;
			for (k = 0; k<n;k++){
				sum += weight(z(k)/c)*phi(i,k)*phi(j,k)/c;
			}
			if ((abs(sum) > epsilon && i !=j) || (i == j && abs(sum - 1) > epsilon)){ /* Test */
				return 0;
			}
		}
	} 
	return 1;
}


/**
 * \file orthonormalite.cpp
 * \brief fonctions de calcul de l'orthonormalité des solutions
 * \author Charles Anteunis
 */


