#ifndef TESTORTHONORMAL_H
#define TESTORTHONORMAL_H

#include "orthonormalite.h"
#include <cxxtest/testSolution.h>

/**
 * \file testOrthonormal.h
 * \brief tests unitaires du calcul de l'orthonormalité des solutions
 *
 * L'orthonormalité est vérifiée par l'intégrale $/integ_{-/infty}^/infty{/psi_m(z)psi_n(z)}dz$
 * \author Charles Anteunis
 */

class TestOrthonormal : public CxxTest::TestSuite
{
public:
    /**
     * \brief test si l'intégrale avec m != n vaut 0
     */
    void test_n_m(void)
    {
        int n = 5;
        arma::Row<float> z = {-1,0,1,2,3,4};
        float m = pow(10,-15);
        float w = pow(10,4);
       TS_ASSERT(integrale(n, z, phi(n, z, m, w, hermite(n, z))), m, w);
    }

    /**
     * \brief test si l'intégrale avec m = n vaut 1
     */
    void test_n_n(void)
    {
        TS_ASSERT(integrale(1,1,3.5) - 1 < pow(10, -7));
    }
};

#endif
